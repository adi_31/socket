import { inChart } from "./functions/inchart";
import { upChart } from "./functions/inchart";

const ctx = document.getElementById("chart").getContext("2d");
const chart = inChart(ctx);
const socket = new WebSocket("ws://localhost:9000");

socket.addEventListener("message", (event) => {
    const datas = JSON.parse(event.data);
    upChart(chart, datas);  
});

