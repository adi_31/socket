import Chart from "chart.js/auto";

const data = [];
const labels = [];
const ctx = document.getElementById("chart").getContext("2d");

export function inChart() {

  return new Chart(ctx, {
      type: "line",
      data: {
        labels,
        datasets: [
          {
            label: "Chart with Socket",
            data,
            fill: true,
            borderColor: "blue",
            tension: 0.1,
          },
        ],
      },
      options: {
        animation: true,
        scales: {
          x: {
            beginAtZero: true,
          },
          y: {
            beginAtZero: true,
          },
        },
      },
  });
}
  
export function upChart(ch, dat) {

  data.push(+dat.value);
  labels.push(dat.time);
  ch.update();
}